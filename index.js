const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');


const {authMiddleware} = require("./middlewares/authMiddleware");
const {NodeCourseError} = require("./utils/errors");

const {authRouter} = require("./routers/authRouter");
const {usersRouter} = require("./routers/usersRouter");
const {truckRouter} = require("./routers/truckRouter");
const {loadsRouter} = require("./routers/loadsRouter");

const app = express();

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', authMiddleware, usersRouter);
app.use('/api/trucks', authMiddleware, truckRouter);
app.use('/api/loads', authMiddleware, loadsRouter);

app.use((req, res, next) => {
  res.status(404).json({message: 'Not found'});
});
app.use((err, req, res, next) => {
  if (err instanceof NodeCourseError) {
    return res.status(err.status).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});


const start = async () => {
  try {
    await mongoose.connect("mongodb+srv://admin:admin@cluster0.ik9e4.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", {
      useNewUrlParser: true, useUnifiedTopology: true,
    });
    app.listen(8080);
    console.log('Started');
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
