const express = require('express');
const {getUserInfo, deleteUserProfile} = require("../services/usersService");
const {InvalidRequestError} = require("../utils/errors");
// eslint-disable-next-line
const router = express.Router();

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.get('/',
  asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const userInfo = await getUserInfo(userId);

    if (!userInfo) {
      throw new InvalidRequestError('No user with such id found!');
    }

    res.json({user: {
        _id: userInfo.id,
        email: userInfo.email,
        role: userInfo.role,
        created_date: userInfo.created_date,
      },
    });
  }));
router.delete('/',
  asyncWrapper(async  (req, res) => {
    const {userId} = req.user;
    await deleteUserProfile(userId);
    res.status(200).json({message: 'Account was deleted!'})
  }))
module.exports = {
  usersRouter: router,
};
