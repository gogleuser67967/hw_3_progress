const express = require("express");
const checkShipperMiddleware = require("../middlewares/checkShipperMiddleware");
const checkDriverMiddleware = require("../middlewares/checkDriverMiddleware");
const {iterateLoadState} = require("../services/loadsService");
const {changeTruckStatusToOL} = require("../services/trucksService");
const {assignLoadToDriver} = require("../services/loadsService");
const {log} = require("../services/loadsService");
const {findFreeTrucks} = require("../services/trucksService");
const {getLoadForShipper} = require("../services/loadsService");
const {updateLoadByID} = require("../services/loadsService");
const {getActiveLoadForDriver} = require("../services/loadsService");
const {getLoadsForDriver} = require("../services/loadsService");
const {getLoadsForShipper} = require("../services/loadsService");
const {addLoadByShipper} = require("../services/loadsService");
const {asyncWrapper} = require("../utils/apiUtils");
const router = express.Router();

router.post('/', checkShipperMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const payload = req.body;
  payload.created_by = userId;
  await addLoadByShipper(payload, userId);
  return res.status(200).json({message: 'Added load'})
}))

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  let loads = [];
  if(req.user.role === "SHIPPER"){
    loads = await getLoadsForShipper(userId);
  }
  if(req.user.role === "DRIVER"){
    loads = await getLoadsForDriver(userId);
  }
  return res.status(200).json({loads});
}));

router.get('/active', checkDriverMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const load = await getActiveLoadForDriver(userId);
  res.status(200).json({load});
}))

router.patch('/:id', checkShipperMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await updateLoadByID(id, userId, req.body);
  return res.status(200).json({message: "Success"});
}))
router.post('/:id/post', checkShipperMiddleware, asyncWrapper(async (req, res) => {
  const loadID = req.params.id;
  const {userId} = req.user;

  const load = await getLoadForShipper(userId, loadID);
  if(!load){
    return res.status(400).json({message: "No load"})
  }
  const truck = await findFreeTrucks({
    x: load.dimensions.height,
    y: load.dimensions.width,
    z: load.dimensions.length,
    payload: load.payload
  });
  if(!truck){
    return res.status(200).json({
      "message": "Load not posted, try again later",
      "driver_found": false
    })
  }
  await assignLoadToDriver(load, truck.assigned_to);
  await changeTruckStatusToOL(truck);
  await log(load, "Load assigned to driver with id " + truck.assigned_to);
  return res.status(200).json({
    "message": "Load was posted",
    "driver_found": true
  });
}))

router.patch('/active/state', checkDriverMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await iterateLoadState(userId);
  return res.status(200).json({message: 'Success'});
}));

module.exports = {
  loadsRouter: router,
};
