const express = require('express');
const checkDriverMiddleware = require("../middlewares/checkDriverMiddleware");
const checkTruckMiddleware = require("../middlewares/checkTruckMiddleware");
const {getActiveTruck} = require("../services/trucksService");
const {deleteTruckByID} = require("../services/trucksService");
const {updateTruckByID} = require("../services/trucksService");

const {getTruckByID} = require("../services/trucksService");
const {getTrucksByDriverID} = require("../services/trucksService");
const {addTruckByDriver} = require("../services/trucksService");
// eslint-disable-next-line
const router = express.Router();

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.post('/',
  asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const {role} = req.user;
    const {type} = req.body;

    if(role !== 'DRIVER'){
      res.status(400).json({message: 'You are not a driver!'})
    }
    await addTruckByDriver(type, userId)
    return res.status(200).json({message: 'Truck added!'})
  }));

router.get('/', checkDriverMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const trucks = await getTrucksByDriverID(userId);
  return res.status(200).json({trucks});
}));

router.get('/:id', checkDriverMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  const truck = await getTruckByID(userId, id);
  if(!truck){
    res.status(400).json({message: 'No truck with such id!'})
  }
  return res.status(200).json({truck});
}))

router.patch('/:id', checkDriverMiddleware, checkTruckMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await updateTruckByID(id, userId, req.body);
  return res.status(200).json({message: "Success"});
}))

router.delete('/:id', checkDriverMiddleware, checkTruckMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await deleteTruckByID(id, userId)
  return res.status(200).json({message: "Success"});
}))

router.post('/:id/assign', checkDriverMiddleware, checkTruckMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const activeTruck = await getActiveTruck(userId);
  if(activeTruck){
    return res.status(400).json({message: 'You already have assigned truck'})
  }

  await updateTruckByID(id, userId, {assigned_to: userId});
  return res.status(200).json({message: "Success"});
}))


module.exports = {
  truckRouter: router,
};
