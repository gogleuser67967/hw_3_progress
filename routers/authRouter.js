const express = require('express');
// eslint-disable-next-line
const router = express.Router();

const {
  registration,
  login,
} = require('../services/authService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.post('/register',
  asyncWrapper(async (req, res) => {
    const {
      email,
      password,
      role
    } = req.body;

    await registration({email, password, role});

    return res.json({message: 'Account created successfully!'});
  }));

router.post('/login', asyncWrapper(async (req, res) => {
  const {
    email,
    password,
  } = req.body;

  const token = await login({email, password});

  return res.json({message: 'Success', jwt_token: token});
}));

module.exports = {
  authRouter: router,
};
