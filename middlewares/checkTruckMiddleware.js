const {getTruckByID} = require("../services/trucksService");

const checkTruckMiddleware = async (req, res, next) => {
  const {userId} = req.user;
  const {id} = req.params;
  const truck = await getTruckByID(userId, id);
  if(!truck){
    return res.status(400).json({message: 'No truck with such id!'})
  }
  next();
}

module.exports = checkTruckMiddleware;
