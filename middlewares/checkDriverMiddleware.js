const checkDriverMiddleware = (req, res, next) => {
  const {role} = req.user;
  if(role !== 'DRIVER'){
    return res.status(400).json({message: 'You are not a driver!'})
  }
  next();
}

module.exports = checkDriverMiddleware;
