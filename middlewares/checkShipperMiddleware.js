const checkShipperMiddleware = (req, res, next) => {
  const {role} = req.user;
  if(role !== 'SHIPPER'){
    return res.status(403).json({message: 'You are not a shipper!'})
  }
  next();
}

module.exports = checkShipperMiddleware;
