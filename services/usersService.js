const {User} = require('../models/User');

const getUserInfo = (userId) => {
  return User.findOne({_id: userId});
};

const deleteUserProfile = async (userId) => {
  await User.findOneAndRemove({_id: userId});
};

const changeUsersPassword = async (userId, newPassword) => {
  await User.findOneAndUpdate({_id: userId}, {$set: {password: newPassword}});
};

module.exports = {
  getUserInfo,
  deleteUserProfile,
  changeUsersPassword,
};
