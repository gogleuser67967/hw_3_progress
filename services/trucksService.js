const {Truck} = require('../models/Truck');

const addTruckByDriver = async (truckType, driverID) => {
  const newTruck = new Truck({
    created_by: driverID,
    type: truckType
  });
  await newTruck.save();
}

const getTrucksByDriverID = (driverID) => {
  return Truck.find({created_by: driverID});
}

const getTruckByID = (driverID, truckID) => {
  return Truck.findOne({_id: truckID, created_by: driverID});
}

const updateTruckByID = async (truckID, driverID, newData) => {
  await Truck.findOneAndUpdate({_id: truckID, created_by: driverID}, newData);
}

const deleteTruckByID = async (truckID, driverID) => {
  await Truck.findOneAndRemove({_id: truckID, created_by: driverID});
}

const getActiveTruck = async (driverID) => {
  return Truck.findOne({assigned_to: driverID})
}

const findFreeTrucks = async (load) => {
  const truckTypes = [
    {
      type: "SPRINTER",
      x: 300,
      y: 250,
      z: 170,
      payload: 1700
    },
    {
      type: "SMALL STRAIGHT",
      x: 500,
      y: 250,
      z: 170,
      payload: 2500
    },
    {
      type: "LARGE STRAIGHT",
      x: 700,
      y: 350,
      z: 200,
      payload: 4000
    },
  ]
  const bestTruck = truckTypes.find(el => {
    return el.x > load.x && el.y > load.y && el.z > load.z && el.payload > load.payload;
  })
  if(!bestTruck){
    return null;
  }
  const trucks = Truck.where('assigned_to').ne(null).where('status').equals('IS').where('type').equals(bestTruck.type).findOne();
  return trucks;
}
const changeTruckStatusToOL = async (truck) => {
  truck.status = "OL";
  await truck.save();
}
module.exports = {
  addTruckByDriver,
  getTrucksByDriverID,
  getTruckByID,
  updateTruckByID,
  deleteTruckByID,
  getActiveTruck,
  findFreeTrucks,
  changeTruckStatusToOL
}
