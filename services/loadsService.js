const {getActiveTruck} = require("./trucksService");
const {Load} = require('../models/Load');

const addLoadByShipper = async (loadPayload, shipperID) => {
  const newLoad = new Load(loadPayload);
  await newLoad.save();
}
const getLoadsForShipper = async (shipperID) => {
  return Load.find({created_by: shipperID})
}
const getLoadsForDriver = async (driverID) => {
  return Load.find({assigned_to: driverID})
}
const getActiveLoadForDriver = async (driverID) => {
  return Load.findOne({assigned_to: driverID, status: 'ASSIGNED'})
}
const iterateLoadState = async (driverID) => {
  const currentLoad = await getActiveLoadForDriver(driverID);
  const currentTruck = await getActiveTruck(driverID);
  const states = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'];
  const currentStateIndex = states.indexOf(currentLoad.state);
  if(currentStateIndex > -1 && currentStateIndex < states.length-1){
    currentLoad.state = states[currentStateIndex+1];
    if(currentLoad.state === 'Arrived to delivery' ){
      currentLoad.status = "SHIPPED";
      currentTruck.status = "IS";
    }
    await currentLoad.save();
    await currentTruck.save();
  }
}
const getLoadForShipper = async (shipperID, loadID) => {
  return Load.findOne({created_by: shipperID, _id: loadID})
}
const updateLoadByID = async (loadID, shipperID, newData) => {
  await Load.findOneAndUpdate({_id: loadID, created_by: shipperID}, newData);
}

const log = async (load, text) => {
  load.logs.push({
    message: text,
    time: new Date()
  });
  await load.save();
}
const assignLoadToDriver = async (load, driverID) => {
  load.assigned_to = driverID;
  load.status = "ASSIGNED";
  await load.save();
}
module.exports = {
  addLoadByShipper,
  getLoadForShipper,
  getLoadsForDriver,
  getLoadsForShipper,
  iterateLoadState,
  getActiveLoadForDriver,
  updateLoadByID,
  log,
  assignLoadToDriver
}
