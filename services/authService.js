const jwt = require('jsonwebtoken');
const bcrypt = require("bcrypt");
const {InvalidRequestError} = require("../utils/errors");

const {User} = require('../models/User');

const registration = async ({email, password, role}) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role
  });
  await user.save();
};

const login = async ({email, password}) => {
  const user = await User.findOne({email});
  if (!(await bcrypt.compare(password, user.password))) {
    throw new InvalidRequestError('Invalid username or password');
  }

  return jwt.sign({
    _id: user._id,
    email: user.email,
    role: user.role
  }, 'SECRET');
};

module.exports = {
  registration,
  login,
};
