const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT']
  },
  status: {
    type: String,
    default: 'IS',
    enum: ['OL', 'IS']
  },
  created_date: {
    type: Date,
    default: Date.now(),
  }
});

module.exports = {Truck};
